# ultrasound-localization-software

Software that performs acoustic localization of ultrasound sources via the Direction of Arrival (DOA).

## Requirements

An environment with python >= 3.6 and the dependencies listed in the `requirements.txt` file in the root directory of this repository.
Run `pip3 install -r requirements.txt` to install all mandatory dependencies.

## Executing the application

Run `python -m ullosoftware`.
