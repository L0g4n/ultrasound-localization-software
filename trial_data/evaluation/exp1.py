# To add a new cell, type '# %%'
# To add a new markdown cell, type '# %% [markdown]'
# %% [markdown]
# # Trial Data Evaluation
#

# %%
import matplotlib.pyplot as plt
import pickle as pl
import numpy as np


# %%
music = "MUSIC"
cssm = "CSSM"
waves = "WAVES"
frida = "FRIDA"
srp = "SRP"
algo_names = ["MUSIC", "CSSM", "WAVES", "FRIDA", "SRP"]

# %% [markdown]
# **Mean Absolute Error**: $$\frac{1}{n} \sum_1^n \lvert y_i - \hat{y}_i  \rvert$$
# %% [markdown]
# ## Experiment 1

# %%
zero_degr_errs = pl.load(open("exp1_6.6cm/0.0°_abs_errors.p", "rb"))
fourtyfive_deg_errs_1 = pl.load(open("exp1_6.6cm/45.0°_abs_errors.p", "rb"))
fourtyfive_deg_errs_2 = pl.load(
    open("exp1_6.6cm/45.0°_abs_errors_another.p", "rb"))

music_errors = np.sort(
    zero_degr_errs[music] + fourtyfive_deg_errs_1[music] + fourtyfive_deg_errs_2[music])
cssm_errors = np.sort(
    zero_degr_errs[cssm] + fourtyfive_deg_errs_1[cssm] + fourtyfive_deg_errs_2[cssm])
waves_errors = np.sort(
    zero_degr_errs[waves] + fourtyfive_deg_errs_1[waves] + fourtyfive_deg_errs_2[waves])
frida_errors = np.sort(
    zero_degr_errs[frida] + fourtyfive_deg_errs_1[frida] + fourtyfive_deg_errs_2[frida])
srp_errors = np.sort(
    zero_degr_errs[srp] + fourtyfive_deg_errs_1[srp] + fourtyfive_deg_errs_2[srp])

me_sum = 0
for me in music_errors:
    me_sum += me
me_mae = np.around(me_sum / 150)
print("MUSIC MAE: ", me_mae)

ce_sum = 0
for ce in cssm_errors:
    ce_sum += ce
ce_mae = np.around(ce_sum / 150)
print("CSSM MAE: ", ce_mae)

we_sum = 0
for we in waves_errors:
    we_sum += we
we_mae = np.around(we_sum / 150)
print("WAVES MAE: ", we_mae)

fe_sum = 0
for fe in frida_errors:
    fe_sum += fe
fe_mae = np.around(fe_sum / 150)
print("FRIDA MAE: ", fe_mae)

se_sum = 0
for se in srp_errors:
    se_sum += se
se_mae = np.around(se_sum / 150)
print("SRP MAE: ", se_mae)

plt.figure(0)
plt.plot(music_errors, len(music_errors) * [1], "x")
plt.title(f"MUSIC MAE: {me_mae}")

plt.figure(1)
plt.plot(cssm_errors, len(cssm_errors) * [1], "x")
plt.title(f"CSSM MAE: {ce_mae}")

plt.figure(2)
plt.plot(waves_errors, len(waves_errors) * [1], "x")
plt.title(f"WAVES MAE: {we_mae}")

plt.figure(3)
plt.plot(frida_errors, len(frida_errors) * [1], "x")
plt.title(f"FRIDA MAE: {fe_mae}")

plt.figure(4)
plt.plot(srp_errors, len(srp_errors) * [1], "x")
plt.title(f"SRP MAE: {se_mae}")

plt.show()


# %%
