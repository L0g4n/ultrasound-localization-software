import numpy as np

from .outdoor_experiment import OutdoorExperiment

"""
WAVELENGTH = c / f

For 4kHz wave: 343 / 4_000 = ca. 8.58cm => less or equal than 4.29cm
"""

TEMPERATURE = 24
SRC_AZIMUTH_REF = [1]


def main():
    exp = OutdoorExperiment(
        temperature=TEMPERATURE, sources_azimuth_ref=np.radians(SRC_AZIMUTH_REF), inter_mic_dist=(.056, .056))
    exp.start_experiment()
