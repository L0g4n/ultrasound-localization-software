import numpy as np
import pyroomacoustics as pra
import serial as ser
import time
import pickle
import copy
import platform
from matplotlib import pyplot as plt
from pyroomacoustics.doa import circ_dist

from .util import calc_sound_speed_in_air


class OutdoorExperiment:

    # use the mic array center as the reference point for measurements
    room_center = [0, 0]

    baudrate = 57_600

    def __init__(self, temperature, sources_azimuth_ref, fs=11.5e3, inter_mic_dist=(.066, .066), num_sources=1):
        self.serial_port = self.determine_serial_port(platform.system())

        self.outdoor_temperature = temperature
        self.fs = fs
        self.inter_mic_dist_lr, self.inter_mic_dist_od = inter_mic_dist
        self.num_sources = num_sources
        self.sources_azimuth_ref = sources_azimuth_ref

        self.c = calc_sound_speed_in_air(self.outdoor_temperature)

        self.nfft = 256  # FFT size for DOA

        # freq range parameter for DOA
        self.freq_range = [3_900, 4_000]

        self.create_mic_geometry()

    def determine_serial_port(self, platform_str):
        if platform_str == "Windows":
            return "COM3"
        elif platform_str == "Darwin":
            return "/dev/cu.usbmodem142401"
        elif platform_str == "Linux":
            return "/dev/ttyACM0"
        else:
            raise NotImplementedError("Unsupported OS!")

    def create_mic_geometry(self):
        # stack array along the second axis such that each column corresponds to coordinates of one single microphone
        self.mic_array = np.c_[
            # x, y, coordinates
            [self.room_center[0] - self.inter_mic_dist_lr / \
                2, self.room_center[1]],  # MIC 3, left
            [self.room_center[0], self.room_center[1] + \
                self.inter_mic_dist_od/2],  # MIC 2, up
            [self.room_center[0], self.room_center[1] - \
                self.inter_mic_dist_od/2],  # MIC 4, down
            [self.room_center[0] + self.inter_mic_dist_lr / \
                2, self.room_center[1]]  # MIC 5, right
        ]

    def get_channel_signals_from_serial(self, serial_dev):
        serial_dev.write(b"csp")
        out = b""

        time.sleep(3)
        while serial_dev.in_waiting > 0:
            out += serial_dev.readline()

        channel0 = []
        channel1 = []
        channel2 = []
        channel3 = []

        output = out.decode("ascii").strip()
        # skip the two other messages and the empty lines
        sample_output = output.splitlines()[4:]
        for line in sample_output:
            _, _, ch0, ch1, ch2, ch3 = line.split()
            channel0.append(ch0)
            channel1.append(ch1)
            channel2.append(ch2)
            channel3.append(ch3)

        channel0 = [int(i) for i in channel0]
        channel1 = [int(i) for i in channel1]
        channel2 = [int(i) for i in channel2]
        channel3 = [int(i) for i in channel3]

        assert (len(channel0) + len(channel1) +
                len(channel2) + len(channel3)) == 4 * self.nfft

        # order channels in such a way that the n-th row matches the n-th column in the mic array
        signal_array = [channel2, channel3, channel0, channel1]

        assert channel0 == signal_array[2] and channel1 == signal_array[
            3] and channel2 == signal_array[0] and channel3 == signal_array[1]

        return np.array(signal_array)

    def start_experiment(self):
        trials = 50

        try:
            with ser.Serial(self.serial_port, self.baudrate, write_timeout=3, rtscts=True, dsrdtr=True) as ser_dev:

                algo_names = ["MUSIC", "CSSM", "WAVES", "FRIDA", "SRP"]

                algo_errors = {}
                absolute_errors = []

                # get signals from physical mics into STFT, respecting the mapping from mic channels to physical microphones!
                # Quote from pyroomacoustics doc:
                # "The output from the convolutions will be summed up at the microphones. The result is stored in the signals attribute of room.mic_array with each row corresponding to one microphone."
                signals = self.get_channel_signals_from_serial(ser_dev)

                for algo in algo_names:
                    for trial in range(trials):

                        # no window, yields worse localization results
                        X = np.array(
                            [pra.transform.analysis(
                                signal, self.nfft, self.nfft // 2).T for signal in signals]
                        )

                        doa = pra.doa.algorithms[algo](
                            self.mic_array, self.fs, self.nfft, c=self.c, num_src=self.num_sources, max_four=4)

                        # performs localization on the frames in X
                        doa.locate_sources(X, freq_range=self.freq_range)
                        abs_error = np.around(np.degrees(
                            circ_dist(self.sources_azimuth_ref[0], doa.azimuth_recon[0])))
                        absolute_errors.append(abs_error)

                        """ doa.polar_plt_dirac(
                            azimuth_ref=self.sources_azimuth_ref)
                        plt.title(algo) """

                    algo_errors[algo] = copy.deepcopy(absolute_errors)
                    absolute_errors.clear()
                # plt.show()

                # print(algo_errors)

                # serialization of the current trial data
                pickle.dump(algo_errors, open(
                    f"trial_data/{np.degrees(self.sources_azimuth_ref[0])}°_abs_another_errors.p", "wb"), pickle.HIGHEST_PROTOCOL
                )
        except OSError as e:
            raise SystemExit(
                f"Criticial error: {str(e)}. Aborting.")
