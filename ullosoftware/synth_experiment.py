import numpy as np
import pyroomacoustics as pra
from scipy.io import wavfile
import matplotlib.pyplot as plt
from .util import create_anechoic_room

# NOTE: Localization with more than two sources with a mic array of size 4 does not work reliably anymore

class SyntheticExperiment:

    def __init__(self, SNR=20, c=343, room_dim=[30, 20], fs=11.5e3, inter_mic_distance=.066, num_sources=2, sources_distance=5):
        self.SNR = SNR
        self.c = c
        self.room_dim = np.array(room_dim)
        self.room_center = self.room_dim / 2
        self.inter_mic_distance = inter_mic_distance
        self.num_sources = num_sources
        self.fs = fs
        self.sources_distance = sources_distance

        self.nfft = 256 # FFT size for DOA

        # freq range parameter for DOA
        self.freq_range = [500, 5_500] # covering the 5 kHz bandwidth of [35, 40] kHz

        self.create_synthetic_room()
        self.create_source_angles()

        self.create_mic_geometry()

        self.room.add_microphone_array(pra.MicrophoneArray(self.mic_array, self.fs))

        SyntheticExperiment.add_source_signals()
        self.add_source_locations()

    def create_synthetic_room(self):
        self.room = create_anechoic_room(self.room_dim, self.fs)

    def create_source_angles(self):
        self.sources_azimuth_ref = np.radians(
            np.random.randint(0, 361, self.num_sources)
        )

    def create_mic_geometry(self):
        self.mic_array = np.array( # each column corresponds to coordinates of a single mic
            [
                [self.room_center[0] - self.inter_mic_distance/2 , self.room_center[0], self.room_center[0], self.room_center[0] + self.inter_mic_distance / 2], # x coordinates
                [self.room_center[1], self.room_center[1] + self.inter_mic_distance/2, self.room_center[1] - self.inter_mic_distance/2, self.room_center[1]] # y coordinates
            ]
        )

    @staticmethod
    def add_source_signals():
        _, SyntheticExperiment.sine35 = wavfile.read("audio/35kHz_sine_wave_subsampled.wav")
        _, SyntheticExperiment.sine36 = wavfile.read("audio/36kHz_sine_wave_subsampled.wav")
        _, SyntheticExperiment.sine37 = wavfile.read("audio/37kHz_sine_wave_subsampled.wav")
        _, SyntheticExperiment.sine38 = wavfile.read("audio/38kHz_sine_wave_subsampled.wav")
        _, SyntheticExperiment.sine39 = wavfile.read("audio/39kHz_sine_wave_subsampled.wav")
        _, SyntheticExperiment.sine40 = wavfile.read("audio/40kHz_sine_wave_subsampled.wav")

    def add_source_locations(self):
        self.room.add_source(self.room_center + self.sources_distance * np.r_[np.cos(self.sources_azimuth_ref[0]), np.sin(self.sources_azimuth_ref[0])], signal=SyntheticExperiment.sine40[:self.nfft])
        self.room.add_source(self.room_center + self.sources_distance * np.r_[np.cos(self.sources_azimuth_ref[1]), np.sin(self.sources_azimuth_ref[1])], signal=SyntheticExperiment.sine35[:self.nfft])

    def start_simulation(self):
        self.room.simulate(self.SNR)

        # no window, yields worse localization results
        X = np.array(
            [pra.transform.analysis(signal, self.nfft, self.nfft // 2).T for signal in self.room.mic_array.signals]
        )

        # Available algorithms: CSSM, FRIDA, MUSIC, SRP-PHAT, TOPS, WAVES,
        # NOTE: Algorithms that rely on subspace decompositon can only recover up to `n_mics - 1` sources
        algo_names = ["MUSIC", "CSSM", "WAVES", "FRIDA"]

        # run the algorithms and plot the results
        for algo_name in algo_names:
            # the max_four parameter is necessary for FRIDA only
            # TODO: change max_four to 4
            doa = pra.doa.algorithms[algo_name](self.mic_array, self.room.fs, self.nfft, c=self.c, num_src=self.num_sources, max_four=self.num_sources)

            # performs localization on the frames in X
            doa.locate_sources(X, freq_range=self.freq_range)

            # option to save figure as pdf here
            doa.polar_plt_dirac(azimuth_ref=self.sources_azimuth_ref)
            plt.title(algo_name)

            # doa.azimuth_recon contains the reconstructed location of the source
            print(algo_name)
            print("\tRecovered azimuth:", np.degrees(np.sort(doa.azimuth_recon)), "degrees.")
            print("\tReal azimuth:", np.degrees(np.sort(self.sources_azimuth_ref)), "degrees.")
        plt.show()
