import pyroomacoustics as pra
import numpy as np
import serial
import time
import itertools

def create_anechoic_room(room_dimensions, fs):
    """
    Create a simulated ShoeBox room.

    Parameters:
    -----------

    - `room_dimensions`: Dimensions of the room
    - `fs`: The sampling rate at which the RIR (of the microphones) will be generated.

    """
    room = pra.ShoeBox(room_dimensions, fs=fs)

    return room

def calc_sound_speed_in_air(temperature):
    return 331.5 + (.6 * temperature) # in m/s

# Credits @fakufaku: https://github.com/LCAV/pyroomacoustics/issues/129#issuecomment-549359559
def inv_sabine(t60, room_dim, c):
    """
    given desired t60, (shoebox) room dimension and sound speed,
    computes the absorption coefficient (amplitude) and image source
    order needed.

    parameters
    ----------
    `t60`: float
        desired t60 (time it takes to go from full amplitude to 60 db decay) in seconds
    `room_dim`: list of floats
        list of length 2 or 3 of the room side lengths
    `c`: float
        speed of sound

    returns
    -------
    `absorption`: float
        the absorption coefficient (in amplitude domain, to be passed to
        room constructor)
    `max_order`: int
        the maximum image source order necessary to achieve the desired t60
    """

    # finding image sources up to a maximum order creates a (possibly 3d) diamond
    # like pile of (reflected) rooms. now we need to find the image source model order
    # so that reflections at a distance of at least up to ``c * rt60`` are included.
    # one possibility is to find the largest sphere (or circle in 2d) that fits in the
    # diamond. this is what we are doing here.
    R = []
    for l1, l2 in itertools.combinations(room_dim, 2):
        R.append(l1 * l2 / np.sqrt(l1 ** 2 + l2 ** 2))

    V = np.prod(room_dim)  # area (2d) or volume (3d)
    # "surface" computation is diff for 2d and 3d
    if len(room_dim) == 2:
        S = 2 * np.sum(room_dim)
        sab_coef = 12  # the sabine's coefficient needs to be adjusted in 2d
    elif len(room_dim) == 3:
        S = 2 * np.sum([l1 * l2 for l1, l2 in itertools.combinations(room_dim, 2)])
        sab_coef = 24

    a2 = sab_coef * np.log(10) * V / (c * S * t60)  # absorption in power (sabine)

    if a2 > 1.0:
        raise ValueError(
            "evaluation of parameters failed. room may be too large for required t60."
        )

    absorption = 1 - np.sqrt(1 - a2)  # convert to amplitude absorption coefficient

    max_order = np.ceil(c * t60 / np.min(R) - 1)

    return absorption, max_order

def open_serial(port="COM3", baudrate=57600, read_timeout=1, write_timeout=5):
    print("Opening serial interface to", port, "...")
    with serial.Serial(port, baudrate=baudrate, timeout=read_timeout, rtscts=True, dsrdtr=True, write_timeout=write_timeout) as ser:
        print("Successfully connected. Please enter your commands below or enter \"exit\" to exit the application.")
        while True:
            input_str = input(">> ")

            if input_str == "exit":
                break
            else:
                try:
                    ser.write(input_str.encode("ascii"))
                except UnicodeEncodeError:
                    print("Please input only ASCII supported characters.")
                    continue

                out = b""

                time.sleep(2)
                while ser.in_waiting > 0:
                    out += ser.readline()

                out = out.decode("ascii").strip()

                if out != '':
                    print(">>", out)
